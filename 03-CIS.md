# CIS Benchmark

1. Get the PDF from https://git.captnemo.in/attachments/1a66cb8f-14d5-4ecd-8d6a-7054e05143e8
2. Understand how it works

# Run it

1. Read through https://github.com/aquasecurity/kube-bench
2. `minikube ssh`
3. `docker run --rm -v `pwd`:/host aquasec/kube-bench:latest`