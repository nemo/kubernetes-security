# security contexts

## References:

- https://kubernetes.io/docs/tasks/configure-pod-container/security-context/
- https://kubernetes.io/docs/concepts/policy/pod-security-policy/

## What to do

1. Create the `attacker.yaml` deployment
2. Go through the https://kubernetes.io/docs/tasks/configure-pod-container/security-context/ task

Skip the bitmasks, but try different flags in the security context and update the deployment to see
what happens with various options.

Try atleast the following:

```
allowPrivilegeEscalation: true
privileged: true
# cd to /dev/ and see after this
readOnlyRootFilesystem: true
# try writing to / after this
runAsGroup
runAsNonRoot
runAsUser
```