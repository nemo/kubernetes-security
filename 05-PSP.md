# Pod Security Policy

## References

- https://kubernetes.io/docs/concepts/policy/pod-security-policy/#what-is-a-pod-security-policy
- https://docs.bitnami.com/kubernetes/how-to/secure-kubernetes-cluster-psp/

```bash
# start minikube with PodSecurityPolicy enabled
minikube start --extra-config=apiserver.GenericServerRunOptions.AdmissionControl=NamespaceLifecycle,LimitRanger,ServiceAccount,PersistentVolumeLabel,DefaultStorageClass,ResourceQuota,DefaultTolerationSeconds,PodSecurityPolicy
```

Go through the example reference at https://kubernetes.io/docs/concepts/policy/pod-security-policy/#example:

```bash
# Set up a namespace and a service account to act as for this example. We’ll use this service account to mock a non-admin user.
kubectl create namespace psp-example
kubectl create serviceaccount -n psp-example fake-user
kubectl create rolebinding -n psp-example fake-editor --clusterrole=edit --serviceaccount=psp-example:fake-user

# To make it clear which user we’re acting as and save some typing, create 2 aliases:
alias kubectl-admin='kubectl -n psp-example'
alias kubectl-user='kubectl --as=system:serviceaccount:psp-example:fake-user -n psp-example'

# Create a policy and a pod (see link for file)
kubectl-admin create -f example-psp.yaml
```