# Kube Auth

## Requirements

- Have `kubectl` working against minikube

## Theory

- https://kubernetes.io/docs/admin/authentication/
- https://jvns.ca/blog/2017/08/05/how-kubernetes-certificates-work/

## Practice

- Figure out how kubernetes authentication works

Try by running the following commands:

- `kubectl config view`
- `kubectl cluster-info`
- `kubectl proxy`
- `minikube dashboard --url`
- `kube auth can-i [verb] [resource]`

Try figuring out what command line flags the minikube
server was started with and use that to figure out what
authentication methods does it support.