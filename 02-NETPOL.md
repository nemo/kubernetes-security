# Network Policies

## Requirements

- Have `kubectl` working against minikube

## References

- https://kubernetes.io/docs/concepts/services-networking/network-policies/
- https://speakerdeck.com/alp/kubernetes-network-policies

# Setup

1. Bring up a new redis server in the `default` namespace  
`kubectl create deployment --image=redis:alpine redis`
2. Create a new namespace:  
`kubectl create namespace attacker`
3. Bring up a new Alpine server  
`kubectl create -f alpine.yml`
4. SSH to the attacker pod  
`kubectl exec -it attacker-pod -n attacker`

# Break it

## Get the IP Address of the Redis Pod

`kubectl describe pod [pod-name]`

## Install redis on the alpine server

`apk update && apk add redis`

## Try accessing redis server

# Fix it