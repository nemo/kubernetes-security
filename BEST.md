# Admission Controllers

## References: 

- https://kubernetes.io/docs/admin/admission-controllers/#podsecuritypolicy

## Pod Security Policy

`kubectl apply -f https://raw.githubusercontent.com/kubernetes/website/master/docs/concepts/policy/restricted-psp.yaml`